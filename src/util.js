import AsyncStorage from '@react-native-community/async-storage';

async function signIn(data) {

    await AsyncStorage.setItem("@RNAuth:user", JSON.stringify(data.user));
    await AsyncStorage.setItem("@RNAuth:token", data.token);
}

async function signOut() {
    await AsyncStorage.clear();
    setUser(null);
}
