import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { View, StyleSheet, Pressable, Text } from 'react-native';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import TextField from './TextField';


const fieldsValidationSchema = yup.object().shape({
    email: yup
        .string()
        .required('O email não pode ser vazio')
        .email('Digite um email válido'),

    first_name: yup
        .string()
        .required('O nome não pode ser vazio'),
    last_name: yup
        .string()
        .required('O sobrenome não pode ser vazio'),

    federal_registration: yup
        .number()
        .required('O CNPJ não pode ser vazio')
        .min(11, 'O peso deve ser menor que 11'),
    latitude: yup
        .string()
        .required('A latitude não pode ser vazia'),
    longitude: yup
        .string()
        .required('A longitude não pode ser vazia'),
    name: yup
        .string()
        .required('O nome do estabelecimento não pode ser vazio'),
    password: yup
        .string()
        .required('A senha não pode ser vazia'),
    password2: yup.string()
        .required('A confirmação não pode ser vazia')
        .oneOf([yup.ref('password'), null], 'A confirmação não bate com a senha'),
});


export default FormComercial = (props) => {

    const { register, setValue, handleSubmit, formState: { errors } } = useForm({
        resolver: yupResolver(fieldsValidationSchema)
    });

    const onSubmit = (data) => props.handleSubmit(data);

    useEffect(() => {
        register('email');
        register('first_name');
        register('last_name');
        register('name');
        register('federal_registration');
        register('latidude');
        register('longitude');
        register('password');
        register('password2');
    }, [register])

    return (
        <View style={{ width: '100%', alignItems: 'center' }}>
            <TextField
                label={'Nome do proprietário'}
                placeholder={'Digite seu nome'}
                error={errors?.first_name}
                onChangeText={text => setValue('first_name', text)}
            />
            <TextField
                label={'Sobrenome do proprietário'}
                placeholder={'Digite seu sobrenome'}
                error={errors?.last_name}
                onChangeText={text => setValue('last_name', text)}
            />
            <TextField
                label={'Email'}
                placeholder={'Digite seu email'}
                error={errors?.email}
                onChangeText={text => setValue('email', text)}
            />
            <TextField
                label={'Nome do estabelecimento'}
                placeholder={'Digite seu o nome do estabelecimento'}
                error={errors?.name}
                onChangeText={text => setValue('name', text)}
            />
            <TextField
                label={'CNPJ'}
                placeholder={'Digite seu CNPJ'}
                error={errors?.federal_registration}
                onChangeText={text => setValue('federal_registration', text)}
                keyboardType="numeric"
            />

            <TextField
                label={'Latitude'}
                placeholder={'Digite a latitude do estabelecimento'}
                error={errors?.latitude}
                keyboardType="numeric"
                onChangeText={text => setValue('latitude', text)}
            />

            <TextField
                label={'Longitude'}
                placeholder={'Digite a longitude do estabelecimento'}
                error={errors?.longitude}
                keyboardType="numeric"
                onChangeText={text => setValue('longitude', text)}
            />

            <TextField
                label={'Senha'}
                placeholder={'Digite sua senha'}
                error={errors?.password}
                secureTextEntry={true}
                onChangeText={text => setValue('password', text)}
            />
            <TextField
                label={'Confirmação de senha'}
                placeholder={'Digite sua confirmação de senha'}
                error={errors?.password2}
                secureTextEntry={true}
                onChangeText={text => setValue('password2', text)}
            />


            <View style={styles.container}>
                <Pressable
                    style={styles.buttonClose}
                    activeOpacity={0.5}
                    onPress={() => props.cancel()}>
                    <Text style={styles.buttonTextStyle}>Cancelar</Text>
                </Pressable>
                <Pressable
                    style={styles.buttonSend}
                    activeOpacity={0.5}
                    onPress={handleSubmit(onSubmit)}>
                    <Text style={styles.buttonTextStyle}>Enviar</Text>
                </Pressable>

            </View >
        </View >
    );
}


const styles = StyleSheet.create({

    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 60,
        width: '80%',
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
    },
    buttonClose: {
        backgroundColor: 'red',
        width: '40%',
        height: 40,
        alignItems: 'center',
    },
    buttonSend: {
        backgroundColor: 'green',
        width: '40%',
        height: 40,
        alignItems: 'center',
    },
});