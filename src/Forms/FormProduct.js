import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { Button, View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import TextField from './TextField';

const fieldsValidationSchema = yup.object().shape({
    name: yup
        .string()
        .required('O nome não pode ser vazio'),
    price: yup
        .number()
        .required('O preço não pode ser vazio'),
})

export default FormProduct = (props) => {
    const { register, setValue, handleSubmit, formState: { errors } } = useForm({
        resolver: yupResolver(fieldsValidationSchema)
    });

    const onSubmit = (data) => props.handleSubmitButton(data);

    useEffect(() => {
        register('name');
        register('price');
    }, [register])

    return (
        <View style={{
            justifyContent: 'center',
            alignContent: 'center',
        }}>
            <TextField
                label={'Nome'}
                placeholder={'Digite o nome do produto'}
                error={errors?.name}
                style={styles.inputStyle}
                onChangeText={text => setValue('name', text)}
            />
            <TextField
                label={'Preço'}
                placeholder={'Digite o preço do produto'}
                error={errors?.price}
                style={styles.inputStyle}
                onChangeText={text => setValue('price', text)}
            />

            <View style={styles.container}>
                <TouchableOpacity
                    style={styles.buttonCancel}
                    activeOpacity={0.5}
                    onPress={() => props.cancel()}>
                    <Text style={styles.buttonTextStyle}>CANCELAR</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.buttonSave}
                    activeOpacity={0.5}
                    onPress={handleSubmit(onSubmit)}>
                    <Text style={styles.buttonTextStyle}>SALVAR</Text>
                </TouchableOpacity>
            </View>

            {/* <Button onPress={handleSubmit(onSubmit)} title="Enviar" />
            <Button onPress={() => props.closeModal()} title="Cancelar" /> */}
        </View >
    );
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    buttonCancel: {
        backgroundColor: 'red',
        width: '40%',
        height: 40,
        alignItems: 'center'
    },
    buttonSave: {
        backgroundColor: 'green',
        width: '40%',
        height: 40,
        alignItems: 'center'
    },
    buttonStyle: {
        backgroundColor: '#7DE24E',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#7DE24E',
        height: 40,
        alignItems: 'center',
        borderRadius: 30,
        marginLeft: 35,
        marginRight: 35,
        marginTop: 20,
        marginBottom: 20,
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
    },
    inputStyle: {
        flex: 1,
        color: '#000',
        paddingLeft: 15,
        paddingRight: 15,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: '#dadae8',
        width: '100%',
        backgroundColor: '#fff'
    },
});