// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/

// Import React and Component
import React, { useState, createRef, useContext } from 'react';
import { HOST } from "@env";
import {
    StyleSheet,
    TextInput,
    View,
    Text,
    ScrollView,
    Image,
    Keyboard,
    TouchableOpacity,
    KeyboardAvoidingView,
} from 'react-native';
import axios from 'axios';
import AuthContext from '../Contexts/AuthContext';
import AsyncStorage from '@react-native-community/async-storage';

import Loader from './Components/Loader';

const LoginScreen = ({ navigation }) => {
    const [userEmail, setUserEmail] = useState('');
    const [userPassword, setUserPassword] = useState('');
    const [loading, setLoading] = useState(false);
    const [errortext, setErrortext] = useState('');

    const { user, setUser, token, setToken, is_seller, setSeller } = useContext(AuthContext);

    const passwordInputRef = createRef();

    const handleSubmitPress = async () => {
        // console.log(HOST);
        setErrortext('');
        if (!userEmail) {
            alert('Insira seu email');
            return;
        }
        if (!userPassword) {
            alert('Insira sua senha');
            return;
        }
        setLoading(true);
        let dataToSend = { username: userEmail, password: userPassword };

        axios.post(`${HOST}/users/get-token/`, dataToSend).then(response => {
            setLoading(false);

            console.log(response.data);
            console.log(response.status);
            // If server response message same as Data Matched
            if (response.status === 200) {
                setLoading(false);

                AsyncStorage.setItem('user_id', JSON.stringify(response.data.user.id));
                setUser(response.data.user);
                AsyncStorage.setItem("@RNAuth:user", JSON.stringify(response.data.user));
                AsyncStorage.setItem("@RNAuth:token", response.data.token);
                // AsyncStorage.setItem("@RNAuth:comercials", JSON.stringify(response.data.users.comercials));
                setToken(response.data.token);

                navigation.replace('DrawerNavigationRoutes');


            } else {
                // setErrortext();
                console.log(response.data);
                // setErrortext(response.msg);
                console.log('Please check your email id or password');
            }
        }).catch(error => {
            console.log('error');
            setLoading(false);
            // console.log(error.response);
            console.log(error.response.data);
            if (error.response) {
                for (var key in error.response.data) {
                    // setFormError(error.response.data.non_field_errors);
                    setErrortext(error.response.data[key][0]);
                    console.log(error.response.data[key][0]);
                }

                // } else if (error.request) {
                //     console.log(error.request);
                // } else {
                //     console.log('Error', error.message);
            }

            setLoading(false);
        });


    };

    return (
        <View style={styles.mainBody}>
            <Loader loading={loading} />
            <ScrollView
                keyboardShouldPersistTaps="handled"
                contentContainerStyle={{
                    flex: 1,
                    justifyContent: 'center',
                    alignContent: 'center',
                }}>
                <View>
                    <KeyboardAvoidingView enabled>
                        <View style={{ alignItems: 'center' }}>
                            <Image
                                source={require('../Image/logo2.png')}
                                style={{
                                    width: '50%',
                                    height: 100,
                                    resizeMode: 'contain',
                                    margin: 30,
                                }}
                            />
                        </View>
                        <View style={styles.SectionStyle}>
                            <TextInput
                                style={styles.inputStyle}
                                onChangeText={(UserEmail) =>
                                    setUserEmail(UserEmail)
                                }
                                placeholder="Enter Email" //dummy@abc.com
                                placeholderTextColor="#8b9cb5"
                                autoCapitalize="none"
                                keyboardType="email-address"
                                returnKeyType="next"
                                onSubmitEditing={() =>
                                    passwordInputRef.current &&
                                    passwordInputRef.current.focus()
                                }
                                underlineColorAndroid="#f000"
                                blurOnSubmit={false}
                            />
                        </View>
                        <View style={styles.SectionStyle}>
                            <TextInput
                                style={styles.inputStyle}
                                onChangeText={(UserPassword) =>
                                    setUserPassword(UserPassword)
                                }
                                placeholder="Senha"
                                placeholderTextColor="#8b9cb5"
                                keyboardType="default"
                                ref={passwordInputRef}
                                onSubmitEditing={Keyboard.dismiss}
                                blurOnSubmit={false}
                                secureTextEntry={true}
                                underlineColorAndroid="#f000"
                                returnKeyType="next"
                            />
                        </View>
                        {errortext != '' ? (
                            <Text style={styles.errorTextStyle}>
                                {errortext}
                            </Text>
                        ) : null}
                        <TouchableOpacity
                            style={styles.buttonStyle}
                            activeOpacity={0.5}
                            onPress={handleSubmitPress}>
                            <Text style={styles.buttonTextStyle}>LOGIN</Text>
                        </TouchableOpacity>
                        <Text
                            style={styles.registerTextStyle}
                            onPress={() => navigation.navigate('PreRegisterScreen')}>
                            Novo aqui? Registre-se
            </Text>
                    </KeyboardAvoidingView>
                </View>
            </ScrollView>
        </View>
    );
};
export default LoginScreen;

const styles = StyleSheet.create({
    mainBody: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#307ecc',
        alignContent: 'center',
    },
    SectionStyle: {
        flexDirection: 'row',
        height: 40,
        marginTop: 20,
        marginLeft: 35,
        marginRight: 35,
        margin: 10,
    },
    buttonStyle: {
        backgroundColor: '#7DE24E',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#7DE24E',
        height: 40,
        alignItems: 'center',
        borderRadius: 30,
        marginLeft: 35,
        marginRight: 35,
        marginTop: 20,
        marginBottom: 25,
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
    },
    inputStyle: {
        flex: 1,
        color: 'white',
        paddingLeft: 15,
        paddingRight: 15,
        borderWidth: 1,
        borderRadius: 30,
        borderColor: '#dadae8',
    },
    registerTextStyle: {
        color: '#FFFFFF',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 14,
        alignSelf: 'center',
        padding: 10,
        width: '200%'
    },
    errorTextStyle: {
        color: 'red',
        textAlign: 'center',
        fontSize: 14,
    },
});