
import React, { useEffect, useState, useContext } from 'react';
import { View, Text, StyleSheet, Modal, Pressable, FlatList, TouchableOpacity } from 'react-native';

import AuthContext from "../../Contexts/AuthContext";


const Item = ({ title, price, idItem }) => (
    <TouchableOpacity style={styles.item} onPress={() => console.log('clicou em ' + idItem)} >
        <Text style={styles.itemText}>{title + ' - ' + price}</Text>
    </TouchableOpacity>
);


const ModalComercial = (props) => {

    const { is_seller, user } = useContext(AuthContext);

    const renderItem = ({ item }) => (
        <Item title={item.name} price={item.price} idItem={item.id} />
    );

    const renderItem2 = ({ item }) => (
        <TouchableOpacity style={styles.item} onPress={() => props.toBuyProduct(item.id)} >
            <Text style={styles.itemText}>{item.name + ' - ' + item.price}</Text>
        </TouchableOpacity>
    );

    useEffect(() => {
    }, [])

    return (
        <Modal
            transparent={true}
            visible={props.visible}
        >
            <View style={styles.centeredView}>
                <View style={styles.modalView}>
                    <Text style={styles.modalText}>{props.comercial.name}</Text>
                    <FlatList
                        data={props.comercial.products}
                        renderItem={renderItem2}
                        style={styles.listProduct}
                        keyExtractor={item => item.id}
                    />

                    <View style={styles.container}>
                        <Pressable
                            style={styles.buttonClose}
                            activeOpacity={0.5}
                            onPress={() => props.closeModal()}>
                            <Text style={styles.buttonTextStyle}>Fechar</Text>
                        </Pressable>
                        {user.is_seller & props.owner ?
                            <Pressable
                                style={styles.buttonNew}
                                activeOpacity={0.5}
                                onPress={() => props.toNewProduct(props.comercial.id)}>
                                <Text style={styles.buttonTextStyle}>Novo Produto</Text>
                            </Pressable>
                            : null}
                    </View>
                </View>

            </View>
        </Modal>)
}

export default ModalComercial;


const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22,
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        width: '90%',
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        height: '60%'
    },
    listProduct: {
        width: '100%',
        backgroundColor: 'white',
        marginBottom: 10,
    },
    button: {
        borderRadius: 20,
        padding: 8,
        elevation: 2
    },
    buttonOpen: {
        backgroundColor: "#F194FF",
    },
    buttonClose: {
        backgroundColor: "#2196F3",
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center",
        fontWeight: 'bold',
        width: '100%'
    },
    item: {
        backgroundColor: '#307ecc',
        padding: 6,
        width: '90%',
        marginVertical: 4,
        marginHorizontal: 12,
    },
    itemText: {
        color: 'white',
        fontSize: 16,
    },
    title: {
        fontSize: 18,
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
    },
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 60,
        width: '100%',
    },
    buttonClose: {
        backgroundColor: 'red',
        width: '40%',
        height: 40,
        alignItems: 'center',
    },
    buttonNew: {
        backgroundColor: 'green',
        width: '40%',
        height: 40,
        alignItems: 'center',
    },
});