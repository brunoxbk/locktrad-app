// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/

// Import React and Component
import React, { useState, useEffect, useContext } from 'react';
import { HOST } from "@env";
import {
    StyleSheet,
    TextInput,
    View,
    Text,
    Image,
    KeyboardAvoidingView,
    Keyboard,
    TouchableOpacity,
    ScrollView,
} from 'react-native';
import axios from 'axios';
import AuthContext from '../../Contexts/AuthContext';
import FormProduct from '../../Forms/FormProduct';
import Loader from '../Components/Loader';


const NewProductScreen = (props) => {
    const [loading, setLoading] = useState(false);
    const { token } = useContext(AuthContext);

    useEffect(() => {
        setLoading(false);
        console.log(HOST);
        //console.log(props.route.params.id);
    }, []);

    const toHome = () => props.navigation.navigate('homeScreenStack');

    const handleSubmitButton = (data) => {
        //Show Loader
        setLoading(true);
        data.marketplace = props.route.params.id;
        console.log(data);
        const url = `${HOST}/marketplace/product/`;

        console.log(url);

        axios.post(url, data).then(response => {
            setLoading(false);

            console.log(response.data);
            console.log(response.status);

            // If server response message same as Data Matched
            if (response.status === 201) {

                console.log(
                    'Registro completo. Por favor faça login!'
                );
                props.navigation.replace('DrawerNavigationRoutes');
            } else {
                // setErrortext();
                console.log(response)
            }
        }).catch(error => {
            console.log('error');
            setLoading(false);
            if (error.response) {
                for (var key in error.response.data) {
                    // setFormError(error.response.data.non_field_errors);
                    //setFormError(error.response.data[key]);
                    console.log(error.response.data[key]);
                }

            } else if (error.request) {
                console.log(error.request);
            } else {
                console.log('Error', error.message);
            }

            setLoading(false);
        });

    };


    return (
        <View style={{ flex: 1, backgroundColor: '#307ecc' }}>
            <Loader loading={loading} />
            <ScrollView
                keyboardShouldPersistTaps="handled"
                contentContainerStyle={{
                    justifyContent: 'center',
                    alignContent: 'center',
                    alignItems: 'center'
                }}>
                <KeyboardAvoidingView enabled>
                    <FormProduct cancel={() => toHome()} handleSubmitButton={(data) => handleSubmitButton(data)} />
                </KeyboardAvoidingView>
            </ScrollView>
        </View>
    );
};
export default NewProductScreen;
