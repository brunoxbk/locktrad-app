// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/

// Import React and Component
import React, { useEffect, useState, useContext } from 'react';
import { View, Text, SafeAreaView, StyleSheet } from 'react-native';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import ModalComercial from '../Components/ModalComercial';
import AuthContext from '../../Contexts/AuthContext';
import { HOST } from "@env";
import axios from 'axios';

const HomeScreen = ({ navigation }) => {
    const [
        markets,
        setMarkets
    ] = useState([]);

    const { user, } = useContext(AuthContext);


    const [
        modalComercial,
        setmodalComercial
    ] = useState(false);

    const [
        owner,
        setOwner
    ] = useState(false);

    const [
        comercial,
        setComercial
    ] = useState({});


    const clickMarket = (market) => {
        setComercial(market);
        setmodalComercial(true);
        // console.log(market.id)
        // console.log(user.marketplaces.map((item) => item.id))
        // console.log(user.marketplaces.map((item) => item.id).includes(market.id))
        setOwner(user.marketplaces.map((item) => item.id).includes(market.id))

    }

    const toNewProduct = (itemId) => {
        console.log(itemId);
        setmodalComercial(false);
        navigation.navigate('newProductStack', { screen: 'NewProductScreen', params: { id: itemId } });
        // navigation.navigate("NewProductScreen", { id: itemId });
    }

    const toBuyProduct = (itemId) => {
        console.log(itemId);
        setmodalComercial(false);
        navigation.navigate('newProductStack', { screen: 'BuyScreen', params: { id: itemId } });
        // navigation.navigate("NewProductScreen", { id: itemId });
    }
    // const toNewProduct = () => navigation.replace('SettingsScreen');


    useEffect(() => {
        console.log(HOST);


        axios.get(`${HOST}/marketplace/`)
            .then((response) => {
                // console.log(response.data);
                setMarkets(response.data);
            })
            .catch((err) => {
                console.error("ops! ocorreu um erro" + err);
            });

    }, []);

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={{ flex: 1, padding: 16 }}>
                <MapView
                    provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                    style={styles.map}
                    region={{
                        latitude: -5.082764,
                        longitude: -42.797682,
                        latitudeDelta: 0.015,
                        longitudeDelta: 0.0121,
                    }}
                >
                    {markets.map((market, index) => (
                        <Marker
                            key={index}
                            coordinate={{ latitude: market.latitude, longitude: market.longitude }}
                            title={market.name}
                            onPress={() => clickMarket(market)}
                            description={''}
                        />
                    ))}
                </MapView>
            </View>
            <ModalComercial
                comercial={comercial}
                visible={modalComercial}
                toNewProduct={toNewProduct}
                toBuyProduct={toBuyProduct}
                owner={owner}
                closeModal={() => setmodalComercial(false)} />
        </SafeAreaView>
    );
};

export default HomeScreen;

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        height: 400,
        width: 400,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
});