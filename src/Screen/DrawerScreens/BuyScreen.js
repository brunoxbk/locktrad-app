// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/

// Import React and Component
import React, { useEffect, useState } from 'react';
import { View, Text, SafeAreaView, Pressable, StyleSheet } from 'react-native';
import axios from 'axios';
import { HOST } from "@env";

const BuyScreen = (props) => {
    const [product, setProduct] = useState({})

    const getProdut = (idItem) => {
        axios.get(`${HOST}/marketplace/product/${idItem}`)
            .then((response) => {
                console.log(response.data);
                setProduct(response.data);
            })
            .catch((err) => {
                console.error("ops! ocorreu um erro" + err);
            });
    }

    useEffect(() => {
        console.log('onResume alternative of android works?');
        console.log(props.route.params);
        getProdut(props.route.params.id);
    });
    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={{ flex: 1, padding: 16 }}>
                <View
                    style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}>
                    <Text
                        style={{
                            fontSize: 20,
                            textAlign: 'center',
                            marginBottom: 16,
                        }}>
                        Nova Compra
            {'\n\n'}
                        {product.name}
                        {'\n\n'}
                        {'R$ ' + product.price}
                    </Text>
                </View>
                <Text
                    style={{
                        fontSize: 18,
                        textAlign: 'center',
                        color: 'grey',
                    }}>
                </Text>
                <Text
                    style={{
                        fontSize: 16,
                        textAlign: 'center',
                        color: 'grey',
                    }}>
                    <Pressable
                        style={styles.buttonStyle}
                        activeOpacity={0.5}>
                        <Text style={styles.buttonTextStyle}>Comprar</Text>
                    </Pressable>
                </Text>
            </View>
        </SafeAreaView>
    );
};

export default BuyScreen;


const styles = StyleSheet.create({

    buttonStyle: {
        backgroundColor: '#7DE24E',
        borderWidth: 0,
        width: 300,
        color: '#FFFFFF',
        borderColor: '#7DE24E',
        height: 40,
        alignItems: 'center',
        borderRadius: 15,
        marginLeft: 45,
        marginRight: 45,
        marginTop: 20,
        marginBottom: 20,
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
        marginLeft: 45,
        marginRight: 45,
    },

});