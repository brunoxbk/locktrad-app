// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/

// Import React and Component
import React, { useState, useEffect, useContext } from 'react';
import { HOST } from "@env";
import {
    StyleSheet,
    View,
    Text,
    Image,
    KeyboardAvoidingView,
    TouchableOpacity,
    ScrollView,
} from 'react-native';
import FormComercial from './../Forms/FormComercial';
import FormCommon from './../Forms/FormCommon';
import Loader from './Components/Loader';
import axios from 'axios';
import AuthContext from '../Contexts/AuthContext';


const RegisterScreen = (props) => {

    const { signed, setSigned } = useContext(AuthContext);

    const [
        isRegistraionSuccess,
        setIsRegistraionSuccess
    ] = useState(false);
    const [loading, setLoading] = useState(false);
    const [errors, setErrors] = useState({});

    useEffect(() => {
        // console.log(props.route.params);
        console.log(HOST);
        // console.log(signed);
    }, []);

    const cancel = () => props.navigation.replace('Auth');

    const handleSubmitButton = (data) => {
        data.is_seller = props.route.params.comercial;
        //Show Loader
        setLoading(true);

        console.log(data);
        const url = `${HOST}/users/${data.is_seller ? 'comercial/register/' : 'register/'}`;

        axios.post(url, data).then(response => {
            setLoading(false);

            // console.log(response.data);
            // console.log(response.status);

            // If server response message same as Data Matched
            if (response.status === 201) {
                setIsRegistraionSuccess(true);
                console.log(
                    'Registration Successful. Please Login to proceed'
                );
            } else {
                // setErrortext();
                console.log(response)
            }
        }).catch(error => {
            console.log('error');
            setLoading(false);
            console.log(error.response.data);
            setErrors(error.response.data);
            // if (error.response) {
            //     for (var key in error.response.data) {
            //         // setFormError(error.response.data.non_field_errors);
            //         //setFormError(error.response.data[key]);
            //         console.log(error.response.data[key]);
            //     }

            // } else if (error.request) {
            //     console.log(error.request);
            // } else {
            //     console.log('Error', error.message);
            // }

            setLoading(false);
        });

    };

    if (isRegistraionSuccess) {
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#307ecc',
                    justifyContent: 'center',
                }}>
                <Image
                    source={require('../Image/success.png')}
                    style={{
                        height: 150,
                        resizeMode: 'contain',
                        alignSelf: 'center'
                    }}
                />
                <Text style={styles.successTextStyle}>
                    Registration Successful
        </Text>
                <TouchableOpacity
                    style={styles.buttonStyle}
                    activeOpacity={0.5}
                    onPress={() => props.navigation.navigate('LoginScreen')}>
                    <Text style={styles.buttonTextStyle}>Login Now</Text>
                </TouchableOpacity>
            </View>
        );
    }

    // justifyContent: 'center',
    // alignContent: 'center',
    // alignItems: 'center',
    // width: '100%',

    return (
        <View style={{ flex: 1, backgroundColor: '#307ecc' }}>
            <Loader loading={loading} />
            <ScrollView
                keyboardShouldPersistTaps="handled"
                contentContainerStyle={{

                }}>
                <View style={{ alignItems: 'center' }}>
                    <Image
                        source={require('../Image/logo2.png')}
                        style={{
                            width: '50%',
                            height: 100,
                            resizeMode: 'contain',
                            margin: 30,
                        }}
                    />
                </View>
                {Object.keys(errors).map((key, index) => (
                    <Text key={'erro' + index} style={styles.errorTextStyle}>
                        {errors[key]}
                    </Text>
                ))}
                {/* <View style={{ alignItems: 'center' }}>
                    <Text style={{ color: '#fff' }}>{props.route.params.comercial ? "Sim" : "Não"}</Text>
                    <Text style={{ color: '#fff' }}>{signed ? "Sim" : "Não"}</Text>
                </View> */}
                <KeyboardAvoidingView enabled>
                    {props.route.params.comercial ? <FormComercial cancel={() => cancel()} handleSubmit={(data) => handleSubmitButton(data)} /> : <FormCommon cancel={() => cancel()} handleSubmit={(data) => handleSubmitButton(data)} />}

                    {/* <TouchableOpacity
                        style={styles.buttonStyle}
                        activeOpacity={0.5}
                        onPress={handleSubmitButton}>
                        <Text style={styles.buttonTextStyle}>REGISTER</Text>
                    </TouchableOpacity> */}

                </KeyboardAvoidingView>
            </ScrollView>
        </View>
    );
};
export default RegisterScreen;

const styles = StyleSheet.create({
    SectionStyle: {
        flexDirection: 'row',
        height: 40,
        marginTop: 20,
        marginLeft: 35,
        marginRight: 35,
        margin: 10,
    },
    buttonStyle: {
        backgroundColor: '#7DE24E',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#7DE24E',
        height: 40,
        alignItems: 'center',
        borderRadius: 30,
        marginLeft: 35,
        marginRight: 35,
        marginTop: 20,
        marginBottom: 20,
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
    },
    inputStyle: {
        flex: 1,
        color: 'white',
        paddingLeft: 15,
        paddingRight: 15,
        borderWidth: 1,
        borderRadius: 30,
        borderColor: '#dadae8',
    },
    errorTextStyle: {
        color: 'red',
        textAlign: 'center',
        fontSize: 14,
    },
    successTextStyle: {
        color: 'white',
        textAlign: 'center',
        fontSize: 18,
        padding: 30,
    },
});