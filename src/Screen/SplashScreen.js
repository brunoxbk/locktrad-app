// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/

// Import React and Component
import React, { useState, useEffect, useContext } from 'react';
import {
    ActivityIndicator,
    View,
    StyleSheet,
    Image
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
import AuthContext from '../Contexts/AuthContext';

const SplashScreen = ({ navigation }) => {
    //State for ActivityIndicator animation
    const [animating, setAnimating] = useState(true);
    const { setUser, setToken, setSeller } = useContext(AuthContext);

    // async function checkSigned(data) {

    //     await AsyncStorage.setItem("@RNAuth:user", JSON.stringify(data.user));
    //     await AsyncStorage.setItem("@RNAuth:token", data.token);
    // }

    async function loadStorageData() {
        const storagedUser = JSON.parse(await AsyncStorage.getItem("@RNAuth:user"));
        const storagedToken = await AsyncStorage.getItem("@RNAuth:token");
        // simular uma lentidão para mostar o loading.
        await new Promise((resolve) => setTimeout(resolve, 2000));

        setAnimating(false);

        console.log(storagedToken);
        console.log(storagedUser);

        if (storagedUser && storagedToken) {
            setUser(storagedUser);
            setToken(storagedToken);
            setSeller(storagedUser.is_seller);
            navigation.replace('DrawerNavigationRoutes');
        } else {
            navigation.replace('Auth');
        }
    }

    useEffect(() => {
        setTimeout(() => {
            // setAnimating(false);
            //Check if user_id is set or not
            //If not then send for Authentication
            //else send to Home Screen
            // AsyncStorage.getItem('user_id').then((value) =>
            //     navigation.replace(
            //         value === null ? 'Auth' : 'DrawerNavigationRoutes'
            //     ),
            // );
            loadStorageData();
        }, 4000);
    }, []);

    return (
        <View style={styles.container}>
            <Image
                source={require('../Image/logo2.png')}
                style={{ width: '90%', resizeMode: 'contain', margin: 30 }}
            />
            <ActivityIndicator
                animating={animating}
                color="#FFFFFF"
                size="large"
                style={styles.activityIndicator}
            />
        </View>
    );
};

export default SplashScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#307ecc',
    },
    activityIndicator: {
        alignItems: 'center',
        height: 80,
    },
});