import React from 'react';


import SplashScreen from '../Screen/SplashScreen';
import DrawerNavigationRoutes from '../Screen/DrawerNavigationRoutes';
import AuthRoutes from './auth';
// import AppRoutes from '../routes/app.routes';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

const Routes = () => {
    return (

        <Stack.Navigator initialRouteName="SplashScreen">
            {/* SplashScreen which will come once for 5 Seconds */}
            <Stack.Screen
                name="SplashScreen"
                component={SplashScreen}
                // Hiding header for Splash Screen
                options={{ headerShown: false }}
            />
            {/* Auth Navigator: Include Login and Signup */}
            <Stack.Screen
                name="Auth"
                component={AuthRoutes}
                options={{ headerShown: false }}
            />
            {/* Navigation Drawer as a landing page */}
            <Stack.Screen
                name="DrawerNavigationRoutes"
                component={DrawerNavigationRoutes}
                // Hiding header for Navigation Drawer
                options={{ headerShown: false }}
            />
        </Stack.Navigator>

    );
};

export default Routes;